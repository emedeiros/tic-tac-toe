<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RequestTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Validate creation of games
     *
     * @return void
     */
    public function testCreateGameTest()
    {
        $this->assertEquals(1, count($this->createGame()));
        $this->assertEquals(2, count($this->createGame()));
        $this->createGame();
        $this->assertEquals(4, count($this->createGame()));
    }

    /**
     * Validating delete API response
     *
     * @return void
     */
    public function testRemoveAGameTest()
    {
        $arrayResponse = $this->createGame();
        $testResponse = $this->delete('/api/match' . $arrayResponse[0]['id']);
        $result = $this->getRespJson($testResponse);
        $this->assertEquals(0, count($result));
    }

    /**
     *
     * Validate a move
     *
     * @return void
     */
    public function testBoardMoveTest()
    {
        $gameCreation = $this->createGame();
        $gameId = $gameCreation[0]['id'];
        //player 1
        $result = $this->callPlay($gameId, 8);
        $this->assertEquals($gameCreation[0]['next'], $result['board'][8]);
    }

    /**
     * Validate a simple game
     *
     * @return void
     */
    public function testSimulatingGamePlayTest()
    {
        $gameCreation = $this->createGame();
        $firstPlayer = $gameCreation[0]['next'];
        $gameId = $gameCreation[0]['id'];
        //player 1
        $this->callPlay($gameId, 0);
        //player 2
        $this->callPlay($gameId, 4);
        //player 1
        $this->callPlay($gameId, 1);
        //player 2
        $this->callPlay($gameId, 5);
        //player 1
        $result = $this->callPlay($gameId, 2);
        $this->assertEquals($firstPlayer, $result['winner']);
    }

    /**
     * Validate blocked feature of next play after end of game
     */
    public function testSimulatePlayingAfterEndingGameTest()
    {
        $gameCreation = $this->createGame();
        $firstPlayer = $gameCreation[0]['next'];
        $gameId = $gameCreation[0]['id'];
        //player 1
        $this->callPlay($gameId, 0);
        //player 2
        $this->callPlay($gameId, 4);
        //player 1
        $this->callPlay($gameId, 1);
        //player 2
        $this->callPlay($gameId, 5);
        //player 1
        $blockedBoard = $this->callPlay($gameId, 2);
        //player 2
        $shouldBeBlocked = $this->callPlay($gameId, 6);
        $this->assertEquals($blockedBoard, $shouldBeBlocked);
    }

    /**
     * Simulating a play
     *
     * @param int $gameId   game id
     * @param int $position position of play
     *
     * @return array
     */
    public function callPlay($gameId, $position)
    {
        return $this->getRespJson(
            $this->json('PUT', '/api/match/' . $gameId, $this->putStructure($position))
        );
    }

    /**
     * Structure for simulating a game play
     *
     * @param $position
     *
     * @return array
     */
    protected function putStructure($position)
    {
        return [
            'position' => $position
        ];
    }
    /**
     * Simple method for creating game
     *
     * @return array
     */
    protected function createGame()
    {
        return $this->getRespJson(
            $this->post('/api/match')
        );
    }

    /**
     * Converting body content into array
     *
     * @param \Illuminate\Foundation\Testing\TestResponse $response comment
     *
     * @return array
     */
    protected function getRespJson($response)
    {
        return json_decode(
            $response->getContent(),
            true
        );
    }
}

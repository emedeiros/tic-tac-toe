<?php

namespace Tests\Unit;

use App\Models\Match;
use App\Services\MatchCreationService;
use App\Services\MatchService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class MatchMoveServiceTest
 *
 * @package Tests\Unit
 */
class MatchCreationServiceTest extends TestCase
{

    /**
     * Service instance
     *
     * @var $service MatchService
     */
    protected $matchService;

    use RefreshDatabase;

    /**
     * Setup before every test
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->matchService = $this->app->make(MatchService::class);
    }


    /**
     * Validate the name of match
     *
     * @return void
     */
    public function testGetMatchNameShouldReturnString()
    {
        $this->assertEquals(
            MatchCreationService::MATCH_PREFIX . '1',
            MatchCreationService::getNewMatchName()
        );
    }

    /**
     * Validate if random player will return one
     *
     * @return void
     */
    public function testGetRandomUserForStartShouldReturnOne()
    {
        $this->assertEquals(1, MatchCreationService::getRandomUserForStart(1, 1));
    }

    /**
     * Validate if method getRandmoUser return two
     *
     * @return void
     */
    public function testGetRandomUserForStartShouldReturnTwo()
    {
        $this->assertEquals(2, MatchCreationService::getRandomUserForStart(2, 2));
    }

    /**
     * Test creation of match
     *
     * @return void
     */
    public function testCreateMatchShouldReturnTrueWhenOk()
    {
        $result = MatchCreationService::createMatch(
            MatchCreationService::getNewMatchName(),
            MatchCreationService::getRandomUserForStart()
        );
        $this->assertNotNull($result);
    }

    /**
     * Test generateMatch should return false when passed Match not saved in db
     * in service
     *
     * @return void
     */
    public function testGenerateMatchPositionsShouldReturnFalse()
    {
        $this->assertFalse(
            MatchCreationService::generateMatchPositions(new Match())
        );
    }

    /**
     * Test getAllPositions should return empty when
     * theres no match in service and theres a match without positions created
     *
     * @return void
     */
    public function testGetAllPositionsShouldReturnEmptyArray()
    {
        $match = MatchCreationService::createMatch('test', 1);
        $this->assertEquals(0, count($match->boardPositions));
    }

    /**
     * Test generateMatch should return true when positions is created
     *
     * @return void
     */
    public function testGeneratePositionShouldReturnTrue()
    {
        $match = MatchCreationService::createMatch('test', 1);
        $this->assertTrue(
            MatchCreationService::generateMatchPositions($match)
        );
        $this->assertEquals(9, count($match->boardPositions));
    }
}

<?php

namespace Tests\Unit;

use App\Services\MatchMoveService;
use App\Services\MatchService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class MatchMoveServiceTest
 *
 * @package Tests\Unit
 */
class MatchMoveServiceTest extends TestCase
{

    /**
     * Service instance
     *
     * @var $service MatchService
     */
    protected $matchService;

    use RefreshDatabase;

    /**
     * Setup before every test
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->matchService = $this->app->make(MatchService::class);
    }

    /**
     * Test move shuld return true when position is free
     *
     * @return void
     */
    public function testMoveShouldReturnTrueWhenPositionIsFree()
    {
        $this->matchService->createPlayableMatch();
        $match = $this->matchService->getMatch();
        $currentPlayer = $match->next;
        $this->assertTrue(MatchMoveService::move($match, 0));
        $this->assertNotEquals($currentPlayer, $match->next);
    }

    /**
     * Test move shuld return true when position is free
     *
     * @return void
     */
    public function testMoveShouldReturnFalseWhenPositionIsOccupied()
    {
        $this->matchService->createPlayableMatch();
        $match = $this->matchService->getMatch();
        MatchMoveService::move($match, 0);
        $this->assertFalse(MatchMoveService::move($match, 0));
    }

    /**
     * Test move should return false when match has a winner
     *
     * @return void
     */
    public function testMoveShouldReturnFalseWhenMatchHasWinner()
    {
        $this->matchService->createPlayableMatch();
        $match = $this->matchService->getMatch();
        $match->winner = 2;
        $this->assertFalse(MatchMoveService::move($match, 0));
    }
}

<?php

namespace Tests\Unit;

use App\Services\MatchRemoveService;
use App\Services\MatchService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class MatchMoveServiceTest
 *
 * @package Tests\Unit
 */
class MatchRemoveServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Service instance
     *
     * @var $service MatchService
     */
    protected $matchService;

    /**
     * Setup before every test
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->matchService = $this->app->make(MatchService::class);
    }

    /**
     * Test remove match should return true when found and removed
     *
     * @return void
     */
    public function testRemoveMatchShouldReturnTrue()
    {
        $this->matchService->createPlayableMatch();
        $match = $this->matchService->getMatch();
        $this->assertTrue(
            MatchRemoveService::remove(
                $match
            )
        );
    }
}

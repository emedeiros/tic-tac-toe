<?php

namespace Tests\Unit;

use App\Models\Match;
use App\Services\MatchService;
use App\Services\MatchWinnerService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class MatchMoveServiceTest
 *
 * @package Tests\Unit
 */
class MatchWinnerServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * MatchWinnerService
     *
     * @var MatchWinnerService
     */
    protected $service;

    /**
     * MatchService
     *
     * @var MatchService
     */
    protected $matchService;

    /**
     * Model match
     *
     * @var Match
     */
    protected $match;
    /**
     * Setup before every test
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->service = $this->app->make(MatchWinnerService::class);
        $this->matchService = $this->app->make(MatchService::class);
        $this->matchService->createPlayableMatch();
        $this->match = $this->matchService->getMatch();
    }

    /**
     * Test checkWinner should return false when passed a new Board
     *
     * @return void
     */
    public function testCheckWinnerShouldReturnFalse()
    {
        $this->assertFalse(
            $this->service->checkWinner($this->match)
        );
    }

    /**
     * Test should return true when one condition to be winner
     * is achieved
     *
     * @return void
     */
    public function testCheckWinnerShouldReturnTrue()
    {
        $winnerId = $this->matchService->getMatch()->next;
        $this->matchService->move(2);
        $this->matchService->move(0);
        $this->matchService->move(4);
        $this->matchService->move(1);
        $this->matchService->move(6);
        $this->assertTrue(
            $this->service->checkWinner($this->match)
        );

        $this->assertEquals(
            $winnerId,
            $this->match->winner
        );

        $this->assertEquals(
            0,
            $this->match->next
        );
    }

    /**
     * Test checkWinner should return false when the match has ended
     *
     * @return void
     */
    public function testCheckWinnerShouldReturnFalseWhenMatchEnded()
    {
        $this->matchService->move(2);
        $this->matchService->move(0);
        $this->matchService->move(4);
        $this->matchService->move(1);
        $this->matchService->move(6);
        $this->match->winner = 1;
        $this->assertFalse(
            $this->service->checkWinner($this->match)
        );
    }
}

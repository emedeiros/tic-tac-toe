<?php

namespace Tests\Unit;

use App\Services\MatchService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class MatchServiceTest
 *
 * @package Tests\Unit
 */
class MatchServiceTest extends TestCase
{

    /**
     * Service instance
     *
     * @var $service MatchService
     */
    protected $service;

    use RefreshDatabase;

    /**
     * Setup before every test
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->service = $this->app->make(MatchService::class);
    }

    /**
     * Test createPlayableMatch should create a playable match
     *
     * @return void
     */
    public function testCreatingPlayableMatch()
    {
        $this->service->createPlayableMatch();
        $this->assertEquals(9, count($this->service->getMatch()->boardPositions));
    }

    /**
     * Test RemoveMatch should return false when match does not exist
     *
     * @return void
     */
    public function testRemoveMatchShouldReturnFalse()
    {
        $this->assertFalse($this->service->removeMatch(100));
    }

    /**
     * Test getPlayableMatches should return empty when there's no
     * register in db
     *
     * @return void
     */
    public function testGetPlayableMatchesShouldReturnEmpty()
    {
        $this->assertEquals(
            0,
            count($this->service->getPlayableMatches())
        );
    }

    /**
     * Test getPlayableMatches should return one match
     *
     * @return void
     */
    public function testgetPlayableMatchesShouldReturnOneMatch()
    {
        $this->assertTrue($this->service->createPlayableMatch());
        $this->assertEquals(
            1,
            count($this->service->getPlayableMatches())
        );
    }

    /**
     * Test getPlayableMatches should return empty after remove match
     *
     * @return void
     */
    public function testgetPlayableMatchesShouldReturnEmptyAfterRemove()
    {
        $this->assertTrue($this->service->createPlayableMatch());
        $this->service->removeMatch(
            $this->service->getMatch()->id
        );

        $this->assertEquals(
            0,
            count($this->service->getPlayableMatches())
        );
    }

    /**
     * Test searchMatch should return false when not found
     * and getMatch should return null
     *
     * @return void
     */
    public function testSearchMatchShouldReturnFalse()
    {
        $this->assertFalse($this->service->searchMatch(100));
        $this->assertNull($this->service->getMatch());
    }

    /**
     * Test searchMatch should return true when found
     * and getMatch should not be null
     *
     * @return void
     */
    public function testSearchMatchShouldReturnTrue()
    {
        $this->service->createPlayableMatch();
        $this->assertTrue(
            $this->service->searchMatch(
                $this->service->getMatch()->id
            )
        );

        $this->assertNotNull($this->service->getMatch());
    }

    /**
     * Test searchMatch should return false when search
     * want a removed match
     * and getMatch should be null
     *
     * @return void
     */
    public function testSearchMatchShouldReturnFalseWhenRemovedMatch()
    {
        $this->service->createPlayableMatch();
        $this->service->removeMatch(
            $this->service->getMatch()->id
        );

        $this->assertFalse(
            $this->service->searchMatch(
                $this->service->getMatch()->id
            )
        );

        $this->assertNull($this->service->getMatch());
    }


    /**
     * Test move should return false when match not selected in service
     *
     * @return void
     */
    public function testMoveShouldReturnFalseWhenMatchNotSelected()
    {
        $this->assertFalse(
            $this->service->move(0)
        );
    }
}

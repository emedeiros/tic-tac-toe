<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardPositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player')
                ->unsigned()
                ->default(0);

            $table->integer('position')
                ->unsigned();

            $table->integer('match_id')
                ->unsigned();

            $table->foreign('match_id')
                ->references('id')
                ->on('matches');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_positions');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Match
 *
 * @package App\Models
 *
 * @property $id int
 * @property $next int
 * @property $name string
 * @property $winner int
 * @property $boardPositions Collection
 * @property $visible bool
 */
class Match extends Model
{

    protected $attributes = [
        'winner' => 0
    ];
    /**
     * Relation between match and boardPosition
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boardPositions()
    {
        return $this->hasMany(BoardPosition::class, 'match_id');
    }
}

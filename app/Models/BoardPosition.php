<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BoardPosition
 *
 * @package App\Models
 *
 * @property $player int
 * @property $position int
 * @property $match_id int
 */
class BoardPosition extends Model
{
    //

    protected $fillable = ['player', 'position'];
    /**
     * Relation between match and position
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function match()
    {
        return $this->hasOne(Match::class, 'match_id');
    }
}

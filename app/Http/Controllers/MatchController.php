<?php

namespace App\Http\Controllers;

use App\Services\MatchService;
use App\Services\ResponseService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;

class MatchController extends Controller
{
    protected $service = null;
    protected $responseService = null;

    /**
     * MatchController constructor.
     *
     * @param MatchService    $business Logic of match.
     * @param ResponseService $res      Structure of response.
     *
     * @return void
     */
    public function __construct(MatchService $business, ResponseService $res)
    {
        $this->service = $business;
        $this->responseService = $res;
    }

    /**
     * First and only page of project
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Returns a list of matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches()
    {
        return $this->returnJsonMatches();
    }

    /**
     * Returns the state of a single match
     *
     * @param int $id Match id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($id)
    {
        return $this->returnJsonMatch((int) $id);
    }

    /**
     * Makes a move in a match
     *
     * @param int $id Match id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function move($id)
    {
        $id = (int) $id;
        $this->service->searchMatch($id);
        $this->service->move((int) Input::get('position'));
        $this->service->checkWinner();
        return $this->returnJsonMatch($id);
    }

    /**
     * Creates a new match and returns the new list of matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $this->service->createPlayableMatch();
        return $this->returnJsonMatches();
    }

    /**
     * Deletes the match and returns the new list of matches
     *
     * @param int $id Match id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $this->service->removeMatch((int) $id);
        return $this->returnJsonMatches();
    }

    /**
     * Return jsonResponse with all matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function returnJsonMatches()
    {
        $matches = $this->service->getPlayableMatches();
        return $this->response(
            $this->responseService->structMatches($matches)
        );
    }

    /**
     * Method that search a Match and return response
     *
     * @param int $id Match id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function returnJsonMatch(int $id)
    {
        $found = $this->service->searchMatch($id);
        if ($found === false) {
            return $this->response(collect([]));
        }

        return $this->response(
            $this->responseService->structMatch(
                $this->service->getMatch()
            )
        );
    }

    /**
     * One method for response
     *
     * @param Collection $data a match or a collection of match
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function response($data)
    {
        return response()->json($data);
    }
}

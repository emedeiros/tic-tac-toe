<?php

namespace App\Services;

use App\Models\Match;

/**
 * Class that check if the match has a winner
 *
 * Class MatchWinnerService
 *
 * @package App\Services
 */
class MatchWinnerService
{
    /**
     * Winner player id
     *
     * @var int|null
     */
    protected $winner = null;
    /**
     * Iterate in match to check if there's a winner
     *
     * @param Match $match Model
     *
     * @return bool
     */
    public function checkWinner(Match $match): bool
    {
        $this->winner = null;
        if ($match->winner !== 0) {
            return false;
        }

        foreach ($this->getBoardConfigsForWinning() as $config) {
            if ($this->checkConfig($match, $config)) {
                return $this->updateWinner($match);
            }
        }
        return false;
    }

    /**
     * Check if the match has a winner in specific config
     *
     * @param Match $match  Model
     * @param array $config Possible win config
     *
     * @return bool
     */
    protected function checkConfig(Match $match, array $config): bool
    {
        $player = null;
        foreach ($config as $position) {
            if ($player === null) {
                $player = $match->boardPositions[$position]->player;
            }
            if ($player !== $match->boardPositions[$position]->player
            ) {
                return false;
            }
        }

        if ($player !== 0) {
            $this->winner = $player;
            return true;
        }

        return false;
    }

    /**
     * Set winner and create an implicit status
     * of ended game (there's no next player)
     *
     * @param Match $match Model
     *
     * @return bool
     */
    protected function updateWinner(Match $match): bool
    {
        $match->winner = $this->winner;
        $match->next = 0;
        return $match->save();
    }

    /**
     * Return all possible configurations for winning a match
     *
     * @return array
     */
    protected function getBoardConfigsForWinning(): array
    {
        return [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];
    }
}

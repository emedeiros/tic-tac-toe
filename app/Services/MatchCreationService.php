<?php

namespace App\Services;

use App\Models\BoardPosition;
use App\Models\Match;

/**
 * Class MatchCreationService
 *
 * @package App\Services
 */
class MatchCreationService
{
    const MATCH_PREFIX = 'Match';

    /**
     * Create a new match in database
     *
     * @param string $name Name for match.
     * @param int    $next Player that will start the match.
     *
     * @return Match
     */
    public static function createMatch(string $name, int $next)
    {
        $match = new Match();
        $match->name = $name;
        $match->next = $next;
        if ($match->save()) {
            return $match;
        }

        return null;
    }

    /**
     * Get match name based on number of records in db
     *
     * @return string
     */
    public static function getNewMatchName(): string
    {
        return self::MATCH_PREFIX . (Match::count() + 1);
    }

    /**
     * Randomly choose one player
     *
     * @param int $player1 Player one Number.
     * @param int $player2 PLayer two number.
     *
     * @return int
     */
    public static function getRandomUserForStart(
        int $player1 = 1,
        int $player2 = 2
    ): int {
        return [$player1, $player2][time() % 2];
    }

    /**
     * Generate all positions from Match
     *
     * @param Match $match Model
     *
     * @return bool
     */
    public static function generateMatchPositions(Match $match): bool
    {
        if ($match->id === null) {
            return false;
        }

        for ($i = 0; $i < 9; $i ++) {
            $match->boardPositions()
                ->save(new BoardPosition(['position' => $i]));
        }

        return true;
    }
}
<?php

namespace App\Services;


use App\Models\BoardPosition;
use App\Models\Match;
use Illuminate\Support\Collection;

/**
 * Class ResponseService
 *
 * @package App\Services
 */
class ResponseService
{
    /**
     * Match array for frontend
     *
     * @param Collection $matches array of Match
     *
     * @return Collection
     */
    public function structMatches(Collection $matches): Collection
    {
        $response = [];
        foreach ($matches as $match) {
            $response[] = $this->structMatch($match);
        }
        return collect($response);
    }

    /**
     * Struct for frontend
     *
     * @param Match $match Model.
     *
     * @return Collection
     */
    public function structMatch(Match $match): Collection
    {
        $board = [];
        /**
         * Type of position
         *
         * @var $position BoardPosition
         */
        foreach ($match->boardPositions as $position) {
            $board[] = $position->player;
        }
        $response = [
            'id' => $match->id,
            'name' => $match->name,
            'next' => $match->next,
            'winner' => $match->winner,
            'board' => $board
        ];

        return collect($response);
    }
}

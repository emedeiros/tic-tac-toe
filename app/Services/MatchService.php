<?php

namespace App\Services;

use App\Models\Match;
use Illuminate\Database\Eloquent\Collection;

/**
 * Service with all business logic for a match.
 *
 * Class MatchService
 *
 * @package App\Services
 */
class MatchService
{
    /**
     * Model from match
     *
     * @var null|Match
     */
    protected $match = null;

    /**
     * Service that check winner
     *
     * @var MatchWinnerService
     */
    protected $winnerService;

    /**
     * MatchService constructor.
     *
     * @param MatchWinnerService $service Service for check winner
     *
     * @return void
     */
    public function __construct(MatchWinnerService $service)
    {
        $this->winnerService = $service;
    }

    /**
     * Creating a playable match with positions
     *
     * @return bool
     */
    public function createPlayableMatch(): bool
    {
        $match = MatchCreationService::createMatch(
            MatchCreationService::getNewMatchName(),
            MatchCreationService::getRandomUserForStart()
        );

        if ($match === null) {
            return false;
        }

        $this->match = $match;
        return MatchCreationService::generateMatchPositions($match);
    }

    /**
     * Return active Matches
     *
     * @return Collection
     */
    public function getPlayableMatches(): Collection
    {
        return $this->matchBasicSearch()
            ->get();
    }

    /**
     * Return every boardPosition
     *
     * @return Match|null
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * Search for a visible match
     *
     * @param int $id Match id
     *
     * @return bool
     */
    public function searchMatch(int $id): bool
    {
        $this->match = $this->matchBasicSearch()
            ->where('id', '=', $id)
            ->first();

        if ($this->match === null) {
            return false;
        }

        return true;
    }

    /**
     * Soft delete from match
     *
     * @param int $id Match id
     *
     * @return bool
     */
    public function removeMatch(int $id): bool
    {
        if ($this->searchMatch($id) === false) {
            return false;
        }

        return MatchRemoveService::remove($this->getMatch());
    }

    /**
     * Move a position in the board
     *
     * @param int $position position in board
     *
     * @return bool
     */
    public function move(int $position): bool
    {
        if ($this->getMatch() === null) {
            return false;
        }

        return MatchMoveService::move($this->getMatch(), $position);
    }

    /**
     * Check if exist a winner
     *
     * @return bool
     */
    public function checkWinner(): bool
    {
        if ($this->getMatch() === null) {
            return false;
        }

        return $this->winnerService->checkWinner(
            $this->getMatch()
        );
    }

    /**
     * Return default search for a match
     *
     * @return Match|\Illuminate\Database\Eloquent\Builder
     */
    protected function matchBasicSearch()
    {
        return Match::with('BoardPositions')
            ->where('visible', '=', true);
    }
}

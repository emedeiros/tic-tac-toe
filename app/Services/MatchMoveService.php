<?php

namespace App\Services;

use App\Models\BoardPosition;
use App\Models\Match;

/**
 * Service with all business logic for a match.
 *
 * Class MatchService
 *
 * @package App\Services
 */
class MatchMoveService
{

    const PLAYER_1 = 1;
    const PLAYER_2 = 2;
    /**
     * Logic of movimentation
     *
     * @param Match $match    Match
     * @param int   $position position requested
     *
     * @return bool
     */
    public static function move(Match $match, int $position): bool
    {
        if ($match->winner !== 0) {
            return false;
        }
        /**
         * Model BoardPosition
         *
         * @var $boardPosition BoardPosition
         */
        foreach ($match->boardPositions as $boardPosition) {
            if ($boardPosition->player === 0
                && $boardPosition->position == $position
            ) {
                return self::updateElements(
                    $match,
                    $boardPosition
                );
            }
        }
        return false;
    }

    /**
     * Method that update Match and BoardMatch
     * to change the player turn
     *
     * @param Match         $match         Model Match
     * @param BoardPosition $boardPosition Model BoardPosition
     *
     * @return bool
     */
    protected static function updateElements(
        Match $match,
        BoardPosition $boardPosition
    ) {
        $boardPosition->player = $match->next;
        if ($boardPosition->save()) {
            $match->next = $match->next == self::PLAYER_1 ?
                self::PLAYER_2 :
                self::PLAYER_1;
            return $match->save();
        }

        return false;
    }
}

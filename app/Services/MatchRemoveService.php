<?php

namespace App\Services;

use App\Models\Match;

/**
 * Class MatchRemoveService
 *
 * @package App\Services
 */
class MatchRemoveService
{

    /**
     * Don't show match for frontend
     *
     * @param Match $match Model
     *
     * @return bool
     */
    public static function remove(Match $match)
    {
        $match->visible = false;
        return $match->save();
    }
}
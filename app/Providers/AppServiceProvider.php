<?php

namespace App\Providers;

use App\Services\MatchService;
use App\Services\MatchWinnerService;
use App\Services\ResponseService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            MatchService::class,
            function ($app) {
                return new MatchService(
                    $app->make(MatchWinnerService::class)
                );
            }
        );

        $this->app->singleton(
            ResponseService::class,
            function ($app) {
                return new ResponseService();
            }
        );

        $this->app->singleton(
            MatchWinnerService::class,
            function ($app) {
                return new MatchWinnerService();
            }
        );
    }
}
